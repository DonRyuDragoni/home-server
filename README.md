# Home Server

These are some small web-based projects that I'm using on my RasPi for some
personal stuff. The full list can be found [here](#projects).

## Instructions

To run this project, you'll need [Racket](https://racket-lang.org/) for the
server code and [Elm](http://elm-lang.org/) to compile the client code.

The build process consists of a single Makefile, and is as simple as:

    $ make client && make run_server

Assuming you have all dependencies installed.

## Projects

- [Library](client/library/README.md)

    Book organizer. Basically, I use it to sort my collection.
