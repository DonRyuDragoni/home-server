# Library

Client side of the library app.

## Build

For now, this acts as a standalone aplication, so, to keep working on it, simply
run `elm-reactor` on this directory and visit
`http://localhost:8000/src/Main.elm` to build and see the project running.
