# Configuration
# =============

# Configurable options for this Makefile, and the only section that's supposed
# to be freely editable.

# The elm compiler's executable.
ELMC := elm-make

# Extra build flags to be passed to the elm-make compiler
ELMFLAGS :=

# Folder (relative to this file) containing the elm source code.
SRCDIR := elm

# The name of the file containing the entrypoint of the elm program, without the
# parent folder or its extension.
MAINSRC := Main

# Folder (relative to this file) where the compiler is supposed to output to.
OUTDIR := out

# Name of the file the compiler is supposed to generate, without the extension
# or its parent folder.
OUTFILE := elm

# Aliasing
# ========

# Folder where the elm package manager keeps dependencies.
ELMSTUFF := elm-stuff

# A glob of all the source files.
SRCS := $(wildcard $(SRCDIR)/*)

# Relative path (from this makefile) to the file containing the entrypoint of
# the program.
ELMMAIN := $(SRCDIR)/$(MAINSRC).elm

# Relative path (from this makefile) to the file the compiler is supposed to
# generate based on the elm code.
DEST := $(OUTDIR)/$(OUTFILE).js

# Build Rules
# ===========

# Default rule: build the $(DEST) file.
.PHONY: all
all: $(DEST)

# Build the $(DEST) file containing the compiled elm code.
#
# The compiler only asks for the main file, but I'd like to rebuild if I change
# something in any other place, hence I list the glob of all souce files here,
# but pass only $(ELMMAIN) to the compiler.
$(DEST): $(SRCS)
	@$(ELMC) $(ELMMAIN) $(ELMFLAGS) --output $@

# Testing Rules
# =============

.PHONY: test
test:
	@echo "Nothing here (yet)"

# Cleaning Rules
# ==============

# By default, delete only the output directory, leaving $(ELMSTUFF) alone.
.PHONY: clean
clean:
	@rm -rf $(OUTDIR)

# Delete the output and the $(ELMSTUFF) directories.
.PHONY: clean_everything
clean_everything: clean
	@rm -rf $(ELMSTUFF)
