import Book exposing (..)

import Debug
import Dialog
import Html exposing (Html, em, text, td, th, tr)
import Html.Attributes exposing (class)
import Html.Events exposing (onClick)
import Http
import Json.Decode as Dec

{-
The state of the program.

- books

  The list containing all the books in the library.

- sortedBy

  Which of the SortOptions is in current use to sort the books.

- showDialog

  Temporary, needed for the AddBook message.

- lastErr

  The last error message recieved. This field only serves to enable printing
  the error, in case one occurs.

- tableHeader

  The columns of the table, corresponding to what elements of each book should
  be displayed and in what order.

-}
type alias Model =
    { books       : List Book
    , lastErr     : Maybe Http.Error
    , showDialog  : Bool
    , sortedBy    : SortOption
    , tableHeader : List SortOption
    }

{-
Send a post request to the server, asking for the list of all books it can
provide.
-}
requestBooks : Http.Request (List Book)
requestBooks =
    Http.post
        "http://localhost:8000/library"
        Http.emptyBody
        (Dec.list Book.decodeBook)

{-
All types of events this program should be able to handle.

- AddBook

  Opens a dialog for the user to enter a new book.

- CloseDialog

  Temporary, because of AddBook.

- RequestBooks

  Make a post request to the server requesting all the books and, when done,
  send a RefreshBooks message.

- RefreshBooks (Result Http.Error (List Book))

  Re-render the book list after RequestBooks is done.

  On success, this is expected to remove any sorting the list of books
  previously had.

- SortBy SortOption

  Re-sort the book list based on the passed option.
-}
type Msg
    = AddBook
    | CloseDialog
    | RequestBooks
    | RefreshBooks (Result Http.Error (List Book))
    | SortBy SortOption

{-
The update loop of the program. For a detailed description of each message, see
the documentation in the Msg type.
-}
update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        AddBook ->
            ( { model
              | showDialog = True
              }
            , Cmd.none
            )

        CloseDialog ->
            ( { model | showDialog = False }
            , Cmd.none
            )

        RefreshBooks (Ok newBooks) ->
            ( { model
              | sortedBy = Unsorted
              , books    = newBooks
              }
            , Cmd.none
            )

        RefreshBooks (Err e) ->
            ( { model
              | lastErr = Just (Debug.log "Error in request" e)
              }
            , Cmd.none
            )

        RequestBooks ->
            ( model
            , Http.send RefreshBooks <| requestBooks
            )

        SortBy field ->
            let
                sortedbooks =
                    model.books
                    |> case field of
                           AuthorName ->
                               Book.sortByAuthorName

                           AuthorSurname ->
                               Book.sortByAuthorSurname

                           Series ->
                               Book.sortBySeries

                           SeriesEntry ->
                               Book.sortBySeriesEntry

                           Title ->
                               Book.sortByTitle

                           -- Unit function: will do nothing
                           Unsorted ->
                               \lb -> lb
            in
                ( { model
                  | books    = sortedbooks
                  , sortedBy = field
                  }
                , Cmd.none
                )

{-
Render the model into HTML elements.
-}
view : Model -> Html Msg
view model =
    let
        {-
        Render each element of the table header, emphasizing it if it happens
        to be the one used to sort the list.
        
        This also makes all the options clickable to trigger the sorting
        process with each defined option.
        -}
        viewHead : SortOption -> Html Msg
        viewHead op =
            let
                {-
                Render a SortOption as a text, emphasizing the one used in the
                sorting process.

                Remember that it is not required to render all SortOptions,
                thus sometimes none will be emph'ed.
                -}
                viewSortOption : SortOption -> Html Msg
                viewSortOption op =
                    if op == model.sortedBy then
                        em [] [text (stringOfSortOption op)]
                    else
                        text (stringOfSortOption op)
            in
                th [onClick (SortBy op)] [viewSortOption op]

        -- The rendered table header.
        tableHeader : Html Msg
        tableHeader =
            tr [class "header"] (List.map viewHead model.tableHeader)

        {-
        Render a book into a row of the table.

        This should use the table header as a guide to output each field in its
        proper place in the table.
        -}
        bookTr : Book -> Html Msg
        bookTr book =
            let
                bookData : SortOption -> Html Msg
                bookData op =
                    td [] [text ((selectorOfSortOption op) book)]
            in
                tr [class "book"] (List.map bookData model.tableHeader)

        -- The rendered body of the table.
        tableBody : List (Html Msg)
        tableBody = List.map bookTr model.books

        addBookDialog : Bool -> Html Msg
        addBookDialog displayDialog =
            Dialog.view
            <| if displayDialog then
                   Just { closeMessage = Just CloseDialog
                        , containerClass = Just "dialog"
                        , header = Just (text "Alert!")
                        , body = Nothing
                        , footer =
                            Just
                            <| Html.button
                                   [onClick CloseDialog]
                                   [text "ok"]
                        }
               else
                   Nothing
    in
        Html.div [class "book-app"]
            [ Html.div [class "book-buttons"]
                       [ Html.button [onClick AddBook] [text "Add Book"]
                       ]

            , Html.div [class "book-table"]
                       [ Html.table [class "book-table"]
                             (tableHeader::tableBody)
                       ]

            , addBookDialog model.showDialog
            ]

{-
The initial state of the program.

- books

  There is no way to know beforehand which books are in the server, so it has
  to start as an empty list.

- sortedBy

  The user hasn't yet requested a sorting option, so use none and display
  everything as it came from the server.

- showDialog

  Temporary, needed for the AddBook message.

- lastErr

  As the program barelly started, there are no errors to be displayed.

- tableHeader

  The default ordering of the columns.

The program is also expected to ask the server for the book data as soon as
possible, so the command is not empty.
-}
init : (Model, Cmd Msg)
init =
    ( { books       = []
      , lastErr     = Nothing
      , showDialog  = False
      , sortedBy    = Unsorted
      , tableHeader = [AuthorName, AuthorSurname, Series, SeriesEntry, Title]
      }
    , Http.send RefreshBooks requestBooks
    )

{-
Register all the functions and start the program.
-}
main : Program Never Model Msg
main =
    Html.program { init = init
                 , view = view
                 , update = update
                 , subscriptions = (always Sub.none)
                 }
