module Book exposing (..)

import Json.Decode as Dec
import Json.Decode.Pipeline as Dec

{-
Aliases to give explicit meaning to the types of the fields in the Author
record.

These are just helpers, thus should not have explicit encoders/decoders.
-}
type alias AuthorName    = String
type alias AuthorSurName = String

{-
What identifies an author.
-}
type alias Author =
    { name : AuthorName
    , sur  : AuthorSurName
    }

{-
Decode an Author record from JSON data.
-}
decodeAuthor : Dec.Decoder Author
decodeAuthor =
    Dec.decode Author
    |> Dec.required "name" Dec.string
    |> Dec.required "sur"  Dec.string

{-
Type aliases to give an explicit meaning for the fields of the Book record.

These are just helpers, thus should not have explicit encoders/decoders.
-}
type alias UniqueId    = Int
type alias Series      = Maybe String
type alias SeriesEntry = Maybe Int
type alias Title       = String

{-
What makes a book a Book.
-}
type alias Book =
    { uid         : UniqueId
    , author      : Author
    , series      : Series
    , seriesEntry : SeriesEntry
    , title       : Title
    }

{-
Decode a Book record from JSON data.
-}
decodeBook : Dec.Decoder Book
decodeBook =
    let
        decodeUniqueId : Dec.Decoder UniqueId
        decodeUniqueId =
            Dec.int

        decodeSeries : Dec.Decoder Series
        decodeSeries =
            Dec.maybe Dec.string

        decodeSeriesEntry : Dec.Decoder SeriesEntry
        decodeSeriesEntry =
            Dec.maybe Dec.int

        decodeTitle : Dec.Decoder Title
        decodeTitle =
            Dec.string
    in
    Dec.decode Book
    |> Dec.required "uid"         decodeUniqueId
    |> Dec.required "author"      decodeAuthor
    |> Dec.optional "series"      decodeSeries      Nothing
    |> Dec.optional "seriesEntry" decodeSeriesEntry Nothing
    |> Dec.required "title"       decodeTitle

{-
The type of all sorting functions contained in this file.
-}
type alias SortFn = List Book -> List Book

{-
Represents all sorting options available for a list of books.
-}
type SortOption
    = AuthorName
    | AuthorSurname
    | Series
    | SeriesEntry
    | Title
    | Unsorted

{-
Sort books by the name of its author.
-}
sortByAuthorName : SortFn
sortByAuthorName =
    List.sortBy
    <| \b -> b.author.name

{-
Sort books by the surname of its author.
-}
sortByAuthorSurname : SortFn
sortByAuthorSurname =
    List.sortBy
    <| \b -> b.author.sur

{-
Sort the books by the name of the series. If a book does not belong in a
series, use its title instead.
-}
sortBySeries : SortFn
sortBySeries =
    List.sortBy
    <| \b ->
        case b.series of
            Nothing ->
                b.title

            Just s ->
                s

{-
Sort books based on its entry number in a series. It it does not belong in one,
use zero.
-}
sortBySeriesEntry : SortFn
sortBySeriesEntry =
    List.sortBy
    <| \b ->
        case b.seriesEntry of
            Nothing ->
                0

            Just n ->
                n

{-
Sort the books by the its titles.
-}
sortByTitle : SortFn
sortByTitle =
    List.sortBy .title

{-
Transform a SortOption into a get function for a Book record, converting that
field into a String when necessary.

For all the Maybe fields of the Book record, selectorOfSortOption will return a
function that'll return an empty string when dealing with the Maybe::Nothing
case.

Since it does not make sense to create a selector from SortOption::Unsorted,
it'll simply create a function that takes an ignored parameter and returns an
empty string.
-}
selectorOfSortOption : SortOption -> (Book -> String)
selectorOfSortOption op =
    case op of
        AuthorName ->
            .author >> .name

        AuthorSurname ->
            .author >> .sur

        Series ->
            .series
            >> \s -> case s of
                Just s -> s
                Nothing -> ""

        SeriesEntry ->
            .seriesEntry
            >> \e -> case e of
                Just e -> toString e
                Nothing -> ""

        Title ->
            .title

        Unsorted ->
            \_ -> ""

{-
Stringfy the SortOption itself, making it able to be rendered into the
document.
-}
stringOfSortOption : SortOption -> String
stringOfSortOption op =
    case op of
        AuthorName ->
            "Author Name"

        AuthorSurname ->
            "Surname"

        Series ->
            "Series"

        SeriesEntry ->
            "Series Entry"

        Title ->
            "Title"

        Unsorted ->
            "Unsorted"
